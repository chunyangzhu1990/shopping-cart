package hautelook;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;


public class Stepdefs {
    private Cart cart = new Cart();

    @Given("^I have an empty cart$")
    public void iHaveAnEmptyCart() throws Throwable {
        this.cart = new Cart();
    }

    @Then("^My subtotal should be \"([^\"]*)\" dollars$")
    public void mySubtotalShouldBeDollars(int subtotal) throws Throwable {
        Assert.assertTrue("Total is " + this .cart.subtotal(), this.cart.subtotal() == subtotal);
    }

    @When("^I add a \"([^\"]*)\" dollar item named \"([^\"]*)\"$")
    public void iAddADollarItemNamed(int itemCost, String productName) throws Throwable {    		
        // Write code here that turns the phrase above into concrete actions
    	Product prod = new Product(productName, itemCost,0);
    	cart.addItem(prod);
        Assert.assertTrue(prod.getName()==productName);
    }

    @Given("^I have a cart with a \"([^\"]*)\" dollar item named \"([^\"]*)\"$")
    public void iHaveACartWithADollarItemNamed(int itemCost, String productName) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
    	Product product = new Product(productName,itemCost,0);   	
    	cart.addItem(product);
    	Assert.assertTrue(this.cart.subtotal()==this.cart.getItemQuntity(product)*itemCost);
    	
    }

    @Then("^My quantity of products named \"([^\"]*)\" should be \"([^\"]*)\"$")
    public void myQuantityOfProductsNamedShouldBe(String productName, int itemCount) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
       // throw new PendingException();
    	Product product = new Product(productName,0,0); 
    	cart.addItem(product);
    	Assert.assertTrue(this.cart.getItemQuntity(product)==itemCount+1);
    }

    @When("^I apply a \"([^\"]*)\" percent coupon code$")
    public void iApplyAPercentCouponCode(int percentOff) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // throw new PendingException();
    	cart.setCoupon(percentOff);
    	Assert.assertTrue(this.cart.subtotal()==10.0*(100.0-percentOff)/100.0);
    }

    @When("^I add a \"([^\"]*)\" dollar \"([^\"]*)\" lb item named \"([^\"]*)\"$")
    public void iAddADollarItemWithWeight(int itemCost, int itemWeight, String productName) throws Throwable {
    	Product prod = new Product(productName, itemCost, itemWeight);
    	cart.addItem(prod);
        Assert.assertTrue(prod.getName()==productName);
        Assert.assertTrue(prod.getPrice()==itemCost);
        Assert.assertTrue(prod.getWeight()==itemWeight);

    }

    @Then("^My total should be \"([^\"]*)\" dollars$")
    public void myTotalShouldBeDollars(int total) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        // throw new PendingException();
    	Assert.assertTrue(cart.total_price()==(double)total);
    }
}
