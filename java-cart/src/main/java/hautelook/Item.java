package hautelook;

// Item class record product quantity in cart

public class Item {

	private Product product;
	private int quantity;

	public Item(Product product) {
		this.setProduct(product);
		quantity = 1;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

}
