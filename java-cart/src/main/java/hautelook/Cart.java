package hautelook;

import java.util.ArrayList;
import java.util.List;

public class Cart {

	List<Item> list = new ArrayList<Item>();
	private double coupon = 0;

	// adding a product into cart
	public void addItem(Product prod) {
		boolean found = false;
		for (Item item : list) {
			if (item.getProduct().getName() == prod.getName()) {
				item.setQuantity(item.getQuantity() + 1);
				found = true;
			}
		}
		if (!found) {
			Item new_item = new Item(prod);
			list.add(new_item);
		}
	}

	// get quantity of a product
	public int getItemQuntity(Product prod) {
		for (Item item : list) {
			if (item.getProduct().getName() == prod.getName()) {
				return item.getQuantity();
			}
		}
		return 0;
	}

	// calculate the subtotal price in cart
	public double subtotal() {
		double res = 0.0;
		for (Item item : list) {
			res += item.getProduct().getPrice() * item.getQuantity() * (100 - coupon) / 100;
		}

		return res;
	}

	// calculate the total price include the shipping fee in different cases
	public double total_price() {
		double total = subtotal();

		if (charge_flat_fee(list)) {
			total += 5;
			return total;
		}

		if (not_charge_flat_fee(list)) {
			double extra_weight = 0;
			for (Item item : list) {
				extra_weight += (item.getProduct().getWeight() - 10) * item.getQuantity();
			}
			total += extra_weight;
			return total;
		}

		if (shipping_free(list)) {
			// total += 0;
		}

		if (total >= 100 && count_ten_pound_or_more(list) > 0) {
			total = total + 20 * count_ten_pound_or_more(list);
		}
		if (total < 100 && count_ten_pound_or_more(list) > 0) {
			total = total + 20 * count_ten_pound_or_more(list) + 5;
		}
		return total;
	}

	// charge flat shipping fee or not
	public boolean charge_flat_fee(List<Item> list) {
		for (Item item : list) {
			if (item.getProduct().getWeight() > 10.0) {
				return false;
			}
		}
		if (subtotal() > 100) {
			return false;
		}
		return true;
	}

	// calculate the price when not charging flat shipping fee
	public boolean not_charge_flat_fee(List<Item> list) {
		for (Item item : list) {
			if (item.getProduct().getWeight() < 10.0) {
				return false;
			}
		}
		if (subtotal() > 100) {
			return false;
		}
		return true;
	}

	// check if free shipping is offered
	public boolean shipping_free(List<Item> list) {
		for (Item item : list) {
			if (item.getProduct().getWeight() > 10.0) {
				return false;
			}
		}
		if (subtotal() < 100) {
			return false;
		}
		return true;
	}

	// count how many items in cart 10 lb or more.
	public int count_ten_pound_or_more(List<Item> list) {
		int count = 0;

		for (Item item : list) {
			if (item.getProduct().getWeight() >= 10.0) {
				count += item.getQuantity();
			}
		}
		return count;
	}

	// get coupon
	public double getCoupon() {
		return coupon;
	}

	// set coupon
	public void setCoupon(double coupon) {
		this.coupon = coupon;
	}

}
